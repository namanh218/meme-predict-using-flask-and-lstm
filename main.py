import os
from predict_meme import predict_meme

from flask import Flask, render_template, request


app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        f = request.files['file']
        f.save(os.path.join(app.root_path, "./static/" + f.filename))
        return render_template('home_page.html', f=f.filename, )

    return render_template('home_page.html')


@app.route('/uploader', methods=['GET', 'POST'])
def upload_file():
    a = (request.args.get('fname'))
    f = (request.args.get('img'))
    a = predict_meme(a)
    return render_template('home_page.html', f=f, a=a)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
